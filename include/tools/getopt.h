#ifndef TOOLS_GETOPT_HEADER_FILE
#define TOOLS_GETOPT_HEADER_FILE

#include "types.h"

enum FlagType
{
  SHORT = 0,
  LONG = 1
};

bool isFlag(char *flag, u8 type);
bool getopt(char *opt, char *shortFlag, char *longFlag);

#endif

