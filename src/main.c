#include "tools/getopt.h"
#include "tools/string.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void help(void)
{
  printf("Usage: mingman [OPTIONS] [PACKAGES]\n");
  printf("\n");
  printf("[OPTIONS]:\n");
  printf("  -p (--package-manager): Custom package manager.\n");
  printf("  -h (--help): Prints this function.\n");
  printf("\n");
  printf("  ...: Flags from choosen package manager or main(pacman).\n");
}

char packageManager[16];

u8 eventFlagHandler(i32 size, char **flag)
{
  u8 i = 0;

  bool bHelp = false;
  bool bPackageManager = false;

  for(i=0; i<size-1; i++)
  {
    bHelp = getopt(flag[i+1], "-h", "--help");
    bPackageManager = getopt(flag[i+1], "-p", "--package-manager");

    switch(bHelp)
    {
      case 0: break;
      default: return 1;
    }

    switch(bPackageManager)
    {
      case 0: break;
      default:
      {
        strcpy(packageManager, flag[i+2]);
        strcpy(flag[i+1], "");
        strcpy(flag[i+2], "");
        return 2;
      }
    }
  }

  return 0;
}

int main(int argc, char **argv)
{
  u8 i = 0,
     stringSize = 128,
     flagArraySize = 16,
     packageArraySize = 255;

  char packagePrefix[128];
  char commandBuffer[stringSize*flagArraySize*packageArraySize],
       flagBuffer[flagArraySize][stringSize],
       packageBuffer[packageArraySize][stringSize];

  char *mingwEnv = getenv("MINGW_PACKAGE_PREFIX");
  if(!mingwEnv)
  {
    return 1;
  }

  strcpy(commandBuffer, "");
  strcpy(packagePrefix, "");
  strcpy(packageManager, "pacman");

  u8 event = eventFlagHandler(argc, argv);

  switch(event)
  {
    case 1: help(); return 0;
    default: break;
  }

  strcat(commandBuffer, packageManager);
  strcat(packagePrefix, mingwEnv);
  strcat(packagePrefix, "-");

  for(i=0; i<argc-1; i++)
  {
    bool bIsFlag = false;

    /* Check for flags (short/long) and append in variable */
    switch(isFlag(argv[i+1], LONG))
    {
      case 1:
      {
        strcpy(flagBuffer[i], argv[i+1]);
        bIsFlag = true;
        break;
      }
      default: break;
    }
    switch(bIsFlag)
    {
      case 0:
      {
        switch(isFlag(argv[i+1], SHORT))
        {
          case 1:
          {
            strcpy(flagBuffer[i], argv[i+1]);
            bIsFlag = true;
            break;
          }
          default: break;
        }
        break;
      }
    }

    /* Append packages in variable */
    switch(bIsFlag)
    {
      case 0:
      {
        switch(_strlen(argv[i+1])-1)
        {
          case 0: break;
          default:
          {
            strcpy(packageBuffer[i], packagePrefix);
            strcat(packageBuffer[i], argv[i+1]);
            break;
          }
        }
        break;
      }
      default: break;
    }
  }

  for(i=0; i<flagArraySize; i++)
  {
    switch(_strlen(flagBuffer[i])-1)
    {
      case 0: break;
      default:
      {
        strcat(commandBuffer, " ");
        strcat(commandBuffer, flagBuffer[i]);
        break;
      }
    }
  }

  for(i=0; i<packageArraySize; i++)
  {
    switch(_strlen(packageBuffer[i])-1)
    {
      case 0: break;
      default:
      {
        strcat(commandBuffer, " ");
        strcat(commandBuffer, packageBuffer[i]);
        break;
      }
    }
  }

  system(commandBuffer);

  return 0;
}

