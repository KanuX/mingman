#include "tools/getopt.h"
#include "tools/string.h"

bool isFlag(char *flag, u8 type)
{
  switch(type)
  {
    case SHORT:
    {
      switch(flag[0])
      {
        case '-': return true;
        default: break;
      }
      break;
    }
    case LONG:
    {
      switch(flag[0])
      {
        case '-':
        {
          switch(flag[1])
          {
            case '-': return true;
            default: break;
          }
          break;
        }
        default: break;
      }
      break;
    }
    default: break;
  }

  return false;
}

bool getopt(char *opt, char *shortFlag, char *longFlag)
{
  bool bIsFlag[2];

  bIsFlag[0] = isFlag(opt, LONG);
  bIsFlag[1] = isFlag(opt, SHORT);

  switch(bIsFlag[0])
  {
    case 1: return flagcmp(opt, longFlag);
    default: break;
  }

  switch(bIsFlag[1])
  {
    case 1: return flagcmp(opt, shortFlag);
    default: break;
  }

  return false;
}

