#include "tools/string.h"

/* Returns the size of the string. */
u16 _strlen(const char *str)
{
  u16 i = 0;
  bool bKeepLoop = true;

  while(bKeepLoop)
  {
    switch(str[i])
    {
      case '\0': bKeepLoop = false; break;
      default: i++; break;
    }
  }

  return i+1;
}

/* Returns 'true' if strings are equal. Returns 'false' if string are not equal. */
bool _strcmp(const char *str1, const char *str2)
{
  u8 i = 0;
  u16 str1len = _strlen(str1),
      str2len = _strlen(str2);

  i8 lenCmp = str1len - str2len;
  switch(lenCmp)
  {
    case 0: break;
    default: return false;
  }

  /* Check if each character equals to each other */
  for(i=0; i<str1len-1; i++)
  {
    i16 char1 = str1[i],
        char2 = str2[i];
    const i8 result = char1 - char2;

    switch(result)
    {
      case 0: break;
      default: return false;
    }
  }

  return true;
}

/* Returns 'true' if flags are equal. Returns 'false' if string is not a flag or not equal. */
bool flagcmp(const char *str1, const char *str2)
{
  switch(str1[0])
  {
    case '-': break;
    default: return false;
  }
  switch(str2[0])
  {
    case '-': break;
    default: return false;
  }

  bool bFlagCmp = _strcmp(str1, str2);

  return bFlagCmp;
}

